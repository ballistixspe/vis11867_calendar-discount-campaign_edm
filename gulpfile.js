// npm install --save-dev browser-sync gulp gulp-load-plugins gulp-twig

var basePaths = {
  src: './dev/',
  dest: './'
};

var paths = {
  templates: {
    src: basePaths.src + 'templates/',
    dest: basePaths.dest
  },
};

var watchFiles = {
  templates: paths.templates.src + '**/*.twig'
};

var gulp = require('gulp');

var plugins = require("gulp-load-plugins")({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /\bgulp[\-.]/
});

var browserSync = require('browser-sync').create(),
  reload = browserSync.reload;
  
// Allows gulp --dev to be run for a more verbose output
var isProduction = true;

gulp.task('templates', function() {
  gulp.src(paths.templates.src + 'index.twig')
    .pipe(plugins.twig())
    .pipe(gulp.dest(paths.templates.dest))
    .pipe(browserSync.stream());
});

gulp.task('watch', ['templates'], function() {
  gulp.watch(watchFiles.templates, ['templates']);
});

gulp.task('serve', ['templates', 'watch'], function() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

gulp.task('default', ['templates']);
